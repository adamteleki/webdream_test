<?php

namespace models;

use core\Validator;
use interfaces\StockInterface;

class Stock implements StockInterface
{
    protected $id;
    private $store_id;
    private $product_id;
    private $quantity;
    protected $rules = [
        [['quantity', 'product_id', 'store_id'], ['integer']]
    ];

    public function addStock($id)
    {
        $product_id = isset($_POST['product_id']) && !empty($_POST['product_id']) && !is_null($_POST['product_id']) ? $_POST['product_id'] : null;
        $quantity_plus = isset($_POST['quantity_plus']) && !empty($_POST['quantity_plus']) && !is_null($_POST['quantity_plus']) ? $_POST['quantity_plus'] : null;
        $stock_id = isset($_POST['stock_id']) && !empty($_POST['stock_id']) && !is_null($_POST['stock_id']) ? $_POST['stock_id'] : null;

        if (!is_null($product_id) && !is_null($quantity_plus)) {

            $original_quantity = $this->getStockById($stock_id)->quantity;
            $new_quantity = 0;
            $storeModel = new Store;

            if ($storeModel->isStoreHaveSpace($id, $quantity_plus)) {
                $new_quantity = ((int)$original_quantity + $quantity_plus);
            } else {
                $storeCapacity = (int)$storeModel->getStoreById($id)->capacity;
                $storeStocks = $this->getStockByStoreId($id);
                $stockQuantity = 0;
                foreach ($storeStocks as $key => $stock) {
                    $stockQuantity += (int)$stock->quantity;
                }
                $quantity_diff = abs(((int)$stockQuantity + (int)$quantity_plus) - $storeCapacity);
                $anotherStore = $storeModel->checkStoresWithFreeSpace($quantity_diff);

                if ($anotherStore !== false) {

                    $this->id = $this->autoIncrement();
                    $this->store_id = $anotherStore;
                    $this->product_id = $product_id;
                    $this->quantity = $quantity_diff;

                    $this->save(true);

                } else {
                    throw new \Exception('All stores are full!');
                }
            }

            $this->id = $stock_id;
            $this->store_id = $id;
            $this->product_id = $product_id;
            $this->quantity = $new_quantity;

            if ($this->save()) {
                return true;
            }

        } else {
            throw new \Exception('There was an error during save! Please check the data you provided!');
        }
        return false;
    }

    public function removeStock($id)
    {
        $product_id = isset($_POST['product_id']) && !empty($_POST['product_id']) && !is_null($_POST['product_id']) ? $_POST['product_id'] : null;
        $quantity_minus = isset($_POST['quantity_minus']) && !empty($_POST['quantity_minus']) && !is_null($_POST['quantity_minus']) ? $_POST['quantity_minus'] : null;
        $stock_id = isset($_POST['stock_id']) && !empty($_POST['stock_id']) && !is_null($_POST['stock_id']) ? $_POST['stock_id'] : null;

        if (!is_null($product_id) && !is_null($quantity_minus)) {

            $original_quantity = $this->getStockById($stock_id)->quantity;
            if (($original_quantity - $quantity_minus) >= 0) {
                $this->id = $stock_id;
                $this->store_id = $id;
                $this->product_id = $product_id;
                $this->quantity = ((int)$original_quantity - $quantity_minus);

                if ($this->save()) {
                    return true;
                }
            } else {
                //TODO: Not enough on stock so we only remove that can possible
            }


        } else {
            throw new \Exception('There was an error during save! Please check the data you provided!');
        }
        return false;
    }

    public function getStocks()
    {
        $stocks = [];
        if (isset($_SESSION['database'])) {
            $database = $_SESSION['database'];
            foreach ($database['inventory'] as $key => $value) {
                $stock = new Stock;
                $stock->id = $value['id'];
                $stock->product_id = $value['product_id'];
                $stock->store_id = $value['store_id'];
                $stock->quantity = $value['quantity'];

                $stocks[$value['id']] = $stock;
            }
        }

        return $stocks;
    }

    public function getStockById($stock_id)
    {
        $stocks = [];
        if (isset($_SESSION['database'])) {
            $database = $_SESSION['database'];
            foreach ($database['inventory'] as $key => $value) {
                if ($stock_id == $value['id']) {
                    $stock = new Stock;
                    $stock->id = $value['id'];
                    $stock->product_id = $value['product_id'];
                    $stock->store_id = $value['store_id'];
                    $stock->quantity = $value['quantity'];

                    $stocks = $stock;
                }
            }
        }
        return $stocks;
    }

    public function getStockByStoreId($store_id = null)
    {
        $stocks = [];
        if (isset($_SESSION['database'])) {
            $database = $_SESSION['database'];
            foreach ($database['inventory'] as $key => $value) {
                $getTrough = false;
                if (!is_null($store_id)) {
                    if ($store_id == $value['store_id']) {
                        $getTrough = true;
                    }
                } else {
                    $getTrough = true;
                }

                if ($getTrough) {
                    $stock = new Stock;
                    $stock->id = $value['id'];
                    $stock->product_id = $value['product_id'];
                    $stock->store_id = $value['store_id'];
                    $stock->quantity = $value['quantity'];

                    $stocks[$value['id']] = $stock;
                }
            }
        }
        return $stocks;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __toString()
    {
        $product_name = '';
        $store_name = '';

        return 'Product:' . $product_name . ' Store: ' . $store_name . ' Quantity: ' . $this->quantity;
    }

    public function save($new = false)
    {
        $validatorArray = [
            'product_id' => $this->product_id,
            'store_id' => $this->store_id,
            'quantity' => $this->quantity
        ];

        $validate = Validator::validate($this->rules, $validatorArray);
        if (is_array($validate)) {
            $ExcaptionString = '';
            foreach ($validate as $validator_key => $validator_value) {
                $ExcaptionString .= $validator_value . PHP_EOL;
            }
            throw new \Exception($ExcaptionString);
        }

        if (isset($_SESSION['database'])) {
            $database = $_SESSION['database'];
            if ($new) {
                $_SESSION['database']['inventory'][] = [
                    'id' => $this->id,
                    'product_id' => $this->product_id,
                    'store_id' => $this->store_id,
                    'quantity' => $this->quantity
                ];
            } else {
                foreach ($database['inventory'] as $key => $value) {
                    if ($this->id == $value['id']) {
                        $database['inventory'][$key]['product_id'] = $this->product_id;
                        $database['inventory'][$key]['store_id'] = $this->store_id;
                        $database['inventory'][$key]['quantity'] = $this->quantity;
                    }
                }
                $_SESSION['database']['inventory'] = $database['inventory'];
            }
        }
        return true;
    }

    public function autoIncrement()
    {
        $ids = [];
        if (isset($_SESSION['database'])) {
            $database = $_SESSION['database'];
            foreach ($database['inventory'] as $key => $value) {
                array_push($ids, $value['id']);
            }
            $latest_id = (int)max($ids);
            $new_id = $latest_id;
            $new_id = $new_id + 1;
            return $new_id;
        }
    }

    public function fillQuantity($store_id, $type = 'add')
    {

    }

}