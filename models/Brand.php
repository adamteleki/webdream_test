<?php

namespace models;

use core\Validator;
use interfaces\BrandInterface;

class Brand implements BrandInterface
{
    protected $id;
    private $name;
    private $quality;
    protected $rules = [
        [['name'], ['string', 'min:4']],
        [['quality'], ['integer', 'minInt:1', 'maxInt:5']]
    ];

    public function createBrand()
    {
        $name = isset($_POST['name']) && !empty($_POST['name']) && !is_null($_POST['name']) ? $_POST['name'] : null;
        $quality = isset($_POST['quality']) && !empty($_POST['quality']) && !is_null($_POST['quality']) ? $_POST['quality'] : null;

        if (!is_null($name) && !is_null($quality)) {
            $this->id = $this->autoIncrement();
            $this->name = $name;
            $this->quality = $quality;
            if ($this->save(true)) {
                return true;
            }
        } else {
            throw new \Exception('There was an error during save! Please check the data you provided!');
        }
        return false;
    }

    public function updateBrand($id)
    {
        $name = isset($_POST['name']) && !empty($_POST['name']) && !is_null($_POST['name']) ? $_POST['name'] : null;
        $quality = isset($_POST['quality']) && !empty($_POST['quality']) && !is_null($_POST['quality']) ? $_POST['quality'] : null;

        if (!is_null($name) && !is_null($quality)) {
            $this->id = $id;
            $this->name = $name;
            $this->quality = $quality;
            if ($this->save()) {
                return true;
            }
        } else {
            throw new \Exception('There was an error during save! Please check the data you provided!');
        }
        return false;
    }

    public function getBrands()
    {
        $brands = [];
        if (isset($_SESSION['database'])) {
            $database = $_SESSION['database'];
            foreach ($database['brands'] as $key => $value) {
                $brand = new Brand;
                $brand->id = $value['id'];
                $brand->name = $value['name'];
                $brand->quality = $value['quality'];

                $brands[$value['id']] = $brand;
            }
        }

        return $brands;
    }

    public function getBrandById($brand_id)
    {
        $brands = [];
        if (isset($_SESSION['database'])) {
            $database = $_SESSION['database'];
            foreach ($database['brands'] as $key => $value) {
                if ($brand_id == $value['id']) {
                    $brand = new Brand;
                    $brand->id = $value['id'];
                    $brand->name = $value['name'];
                    $brand->quality = $value['quality'];

                    $brands = $brand;
                }
            }
        }

        return $brands;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __toString()
    {
        return 'Name:' . $this->name . ' Quality: ' . $this->quality;
    }

    public function save($new = false)
    {
        $validatorArray = [
            'name' => $this->name,
            'quality' => $this->quality
        ];

        $validate = Validator::validate($this->rules, $validatorArray);
        if (is_array($validate)) {
            $ExcaptionString = '';
            foreach ($validate as $validator_key => $validator_value) {
                $ExcaptionString .= $validator_value . PHP_EOL;
            }
            throw new \Exception($ExcaptionString);
        }

        if (isset($_SESSION['database'])) {
            $database = $_SESSION['database'];
            if ($new) {
                $_SESSION['database']['brands'][] = [
                    'id' => $this->id,
                    'name' => $this->name,
                    'quality' => $this->quality
                ];
            } else {
                foreach ($database['brands'] as $key => $value) {
                    if ($this->id == $value['id']) {
                        $database['brands'][$key]['name'] = $this->name;
                        $database['brands'][$key]['quality'] = $this->quality;
                    }
                }
                $_SESSION['database']['brands'] = $database['brands'];
            }
        }
        return true;
    }

    public function autoIncrement()
    {
        $ids = [];
        if (isset($_SESSION['database'])) {
            $database = $_SESSION['database'];
            foreach ($database['brands'] as $key => $value) {
                array_push($ids, $value['id']);
            }
            $latest_id = (int)max($ids);
            $new_id = $latest_id;
            $new_id = $new_id + 1;
            return $new_id;
        }
    }

}