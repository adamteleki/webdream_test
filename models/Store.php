<?php

namespace models;

use core\Validator;
use interfaces\StoreInterface;

class Store implements StoreInterface
{
    protected $id;
    private $name;
    private $capacity;
    protected $rules = [
        [['name'], ['string', 'min:4']],
        [['capacity'], ['integer', 'minInt:1']]
    ];

    public function createStore()
    {
        $name = isset($_POST['name']) && !empty($_POST['name']) && !is_null($_POST['name']) ? $_POST['name'] : null;
        $capacity = isset($_POST['capacity']) && !empty($_POST['capacity']) && !is_null($_POST['capacity']) ? $_POST['capacity'] : null;

        if (!is_null($name) && !is_null($capacity)) {
            $this->id = $this->autoIncrement();
            $this->name = $name;
            $this->capacity = $capacity;
            if ($this->save(true)) {
                return true;
            }
        } else {
            throw new \Exception('There was an error during save! Please check the data you provided!');
        }
        return false;
    }

    public function updateStore($id)
    {
        $name = isset($_POST['name']) && !empty($_POST['name']) && !is_null($_POST['name']) ? $_POST['name'] : null;
        $capacity = isset($_POST['capacity']) && !empty($_POST['capacity']) && !is_null($_POST['capacity']) ? $_POST['capacity'] : null;

        if (!is_null($name) && !is_null($capacity)) {
            $this->id = $id;
            $this->name = $name;
            $this->capacity = $capacity;
            if ($this->save()) {
                return true;
            }
        } else {
            throw new \Exception('There was an error during save! Please check the data you provided!');
        }
        return false;
    }

    public function getStores()
    {
        $stores = [];
        if (isset($_SESSION['database'])) {
            $database = $_SESSION['database'];
            foreach ($database['stores'] as $key => $value) {
                $store = new Store;
                $store->id = $value['id'];
                $store->name = $value['name'];
                $store->capacity = $value['capacity'];

                $stores[$value['id']] = $store;
            }
        }

        return $stores;
    }

    public function getStoreById($store_id)
    {
        $stores = [];
        if (isset($_SESSION['database'])) {
            $database = $_SESSION['database'];
            foreach ($database['stores'] as $key => $value) {
                if ($store_id == $value['id']) {
                    $store = new Store;
                    $store->id = $value['id'];
                    $store->name = $value['name'];
                    $store->capacity = $value['capacity'];

                    $stores = $store;
                }
            }
        }

        return $stores;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __toString()
    {
        return 'Name:' . $this->name . ' Capacity: ' . $this->capacity;
    }

    public function save($new = false)
    {

        $validatorArray = [
            'name' => $this->name,
            'capacity' => $this->capacity
        ];

        $validate = Validator::validate($this->rules, $validatorArray);
        if (is_array($validate)) {
            $ExcaptionString = '';
            foreach ($validate as $validator_key => $validator_value) {
                $ExcaptionString .= $validator_value . PHP_EOL;
            }
            throw new \Exception($ExcaptionString);
        }

        if (isset($_SESSION['database'])) {
            $database = $_SESSION['database'];
            if ($new) {
                $_SESSION['database']['stores'][] = [
                    'id' => $this->id,
                    'name' => $this->name,
                    'capacity' => $this->capacity
                ];
            } else {
                foreach ($database['stores'] as $key => $value) {
                    if ($this->id == $value['id']) {
                        $database['stores'][$key]['name'] = $this->name;
                        $database['stores'][$key]['capacity'] = $this->capacity;
                    }
                }
                $_SESSION['database']['stores'] = $database['stores'];
            }
        }
        return true;
    }

    public function autoIncrement()
    {
        $ids = [];
        if (isset($_SESSION['database'])) {
            $database = $_SESSION['database'];
            foreach ($database['stores'] as $key => $value) {
                array_push($ids, $value['id']);
            }
            $latest_id = (int)max($ids);
            $new_id = $latest_id;
            $new_id = $new_id + 1;
            return $new_id;
        }
    }

    public function isStoreHaveSpace($store_id, $quantity = null)
    {
        $store = $this->getStoreById($store_id);
        $capacity = (int)$store->capacity;

        $stockModel = new Stock;
        $storeStocks = $stockModel->getStockByStoreId($store_id);

        $stockQuantity = 0;
        foreach ($storeStocks as $key => $stock) {
            $stockQuantity += (int)$stock->quantity;
        }

        if (!is_null($quantity)) {
            $stockQuantity += (int)$quantity;
        }

        if ($capacity > $stockQuantity) {
            return true;
        }


        return false;

    }

    public function checkStoresWithFreeSpace($store_id = null, $quantity = null)
    {
        $stores = $this->getStores();
        $storesArray = [];

        foreach ($stores as $key => $store) {
            if (!is_null($store_id)) {
                if ($store_id == $store->id) {
                    continue;
                }
            }
            if ($this->isStoreHaveSpace($store->id, $quantity)) {
                array_push($storesArray, $store->id);
            }
        }

        if (!empty($storesArray)) {
            return $storesArray[0];
        }

        return false;
    }


}