<?php

namespace models;

use core\Validator;
use interfaces\ProductInterface;

class Product implements ProductInterface
{
    protected $id;
    private $sku;
    private $name;
    private $price;
    private $brand_id;
    protected $rules = [
        [['name', 'sku'], ['string', 'min:3']],
        [['price', 'brand_id'], ['integer', 'minInt:1']]
    ];

    public function createProduct()
    {
        $name = isset($_POST['name']) && !empty($_POST['name']) && !is_null($_POST['name']) ? $_POST['name'] : null;
        $sku = isset($_POST['sku']) && !empty($_POST['sku']) && !is_null($_POST['sku']) ? $_POST['sku'] : null;
        $name = isset($_POST['name']) && !empty($_POST['name']) && !is_null($_POST['name']) ? $_POST['name'] : null;
        $brand = isset($_POST['brand']) && !empty($_POST['brand']) && !is_null($_POST['brand']) && is_numeric($_POST['brand']) ? $_POST['brand'] : null;
        $price = isset($_POST['price']) && !empty($_POST['price']) && !is_null($_POST['price']) && is_numeric($_POST['price']) ? $_POST['price'] : null;

        if (!is_null($name) && !is_null($sku) && !is_null($brand) && !is_null($price)) {
            $this->id = $this->autoIncrement();
            $this->name = $name;
            $this->price = $price;
            $this->brand_id = $brand;
            $this->sku = $sku;
            if ($this->save(true)) {
                return true;
            }
        } else {
            throw new \Exception('There was an error during save! Please check the data you provided!');
        }
        return false;
    }

    public function updateProduct($id)
    {
        $name = isset($_POST['name']) && !empty($_POST['name']) && !is_null($_POST['name']) ? $_POST['name'] : null;
        $sku = isset($_POST['sku']) && !empty($_POST['sku']) && !is_null($_POST['sku']) ? $_POST['sku'] : null;
        $name = isset($_POST['name']) && !empty($_POST['name']) && !is_null($_POST['name']) ? $_POST['name'] : null;
        $brand = isset($_POST['brand']) && !empty($_POST['brand']) && !is_null($_POST['brand']) && is_numeric($_POST['brand']) ? $_POST['brand'] : null;
        $price = isset($_POST['price']) && !empty($_POST['price']) && !is_null($_POST['price']) && is_numeric($_POST['price']) ? $_POST['price'] : null;

        if (!is_null($name) && !is_null($sku) && !is_null($brand) && !is_null($price)) {
            $this->id = $id;
            $this->name = $name;
            $this->price = $price;
            $this->brand_id = $brand;
            $this->sku = $sku;
            if ($this->save()) {
                return true;
            }
        } else {
            throw new \Exception('There was an error during save! Please check the data you provided!');
        }
        return false;
    }

    public function getProducts()
    {
        $products = [];
        if (isset($_SESSION['database'])) {
            $database = $_SESSION['database'];
            foreach ($database['products'] as $key => $value) {
                $product = new Product;
                $product->id = $value['id'];
                $product->name = $value['name'];
                $product->price = $value['price'];
                $product->brand_id = $value['brand_id'];
                $product->sku = $value['sku'];

                $products[$value['id']] = $product;
            }
        }

        return $products;
    }

    public function getProductById($product_id)
    {
        $products = [];
        if (isset($_SESSION['database'])) {
            $database = $_SESSION['database'];
            foreach ($database['products'] as $key => $value) {
                if ($product_id == $value['id']) {
                    $product = new Product;
                    $product->id = $value['id'];
                    $product->name = $value['name'];
                    $product->price = $value['price'];
                    $product->brand_id = $value['brand_id'];
                    $product->sku = $value['sku'];

                    $products = $product;
                }
            }
        }

        return $products;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __toString()
    {
        $brandModel = new Brand();
        $brand_name = $brandModel->getBrandById($this->brand_id)->name;
        return 'Name:' . $this->name . ' SKU: ' . $this->sku . ' Brand:' . $brand_name . ' Price: ' . $this->priceFormat($this->price);
    }

    public function save($new = false)
    {
        $validatorArray = [
            'name' => $this->name,
            'sku' => $this->sku,
            'brand_id' => $this->brand_id,
            'price' => $this->price,
        ];

        $validate = Validator::validate($this->rules, $validatorArray);
        if (is_array($validate)) {
            $ExcaptionString = '';
            foreach ($validate as $validator_key => $validator_value) {
                $ExcaptionString .= $validator_value . PHP_EOL;
            }
            throw new \Exception($ExcaptionString);
        }

        if (isset($_SESSION['database'])) {
            $database = $_SESSION['database'];
            if ($new) {
                $_SESSION['database']['products'][] = [
                    'id' => $this->id,
                    'name' => $this->name,
                    'sku' => $this->sku,
                    'brand_id' => $this->brand_id,
                    'price' => $this->price
                ];
            } else {
                foreach ($database['products'] as $key => $value) {
                    if ($this->id == $value['id']) {
                        $database['products'][$key]['name'] = $this->name;
                        $database['products'][$key]['sku'] = $this->sku;
                        $database['products'][$key]['price'] = $this->price;
                        $database['products'][$key]['brand_id'] = $this->brand_id;
                    }
                }
                $_SESSION['database']['products'] = $database['products'];
            }
        }
        return true;
    }

    public function autoIncrement()
    {
        $ids = [];
        if (isset($_SESSION['database'])) {
            $database = $_SESSION['database'];
            foreach ($database['products'] as $key => $value) {
                array_push($ids, $value['id']);
            }
            $latest_id = (int)max($ids);
            $new_id = $latest_id;
            $new_id = $new_id + 1;
            return $new_id;
        }
    }

    public function priceFormat($price)
    {
        return number_format($price) . ' HUF';
    }

}