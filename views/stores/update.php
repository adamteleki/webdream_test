<?php

use models\Store;

$id = isset($_GET['id']) && !empty($_GET['id']) && !is_null($_GET['id']) ? $_GET['id'] : null;
$storeObject = new Store;
$store = $storeObject->getStoreById($id);

if (!empty($_POST) && !is_null($_POST)){
    if ($storeObject->updateStore($id)) {
        header('Location: /?page=stores/index');
    }
}else
?>
<div>
    <form method="POST">
        Name:<br>
        <input type="text" name="name" value="<?= $store->name ?>" required minlength="2">
        <br>
        Quality:<br>
        <input type="number" name="capacity" value="<?= $store->capacity ?>" required min="1">
        <br><br>
        <input type="submit" value="Submit">
    </form>
</div>