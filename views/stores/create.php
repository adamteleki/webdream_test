<?php

use models\Store;

if (!empty($_POST) && !is_null($_POST)) {
    $brand = new Store;
    if ($brand->createStore()) {
        header('Location: /?page=stores/index');
    }
}
?>
<div>
    <form method="POST">
        Name:<br>
        <input type="text" name="name" required minlength="2">
        <br>
        Capacity:<br>
        <input type="number" name="capacity" required min="1">
        <br><br>
        <input type="submit" value="Submit">
    </form>
</div>