<?php

use models\Store;

$storeModel = new Store;
$stores = $storeModel->getStores();
?>
<div class="">
    <h3>Stores</h3>
    <div>
        <a href="/?page=stores/create">New Store</a>
    </div>
    <br/>
    <table style="width:100%" border="collapse">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Capacity</th>
            <th></th>
        </tr>
        <?php
        foreach ($stores as $key => $store) {
            ?>
            <tr>
                <td><?= $store->id ?></td>
                <td><?= $store->name ?></td>
                <td><?= $store->capacity ?></td>
                <td>
                    <div>
                        <a href="/?page=stores/update&id=<?= $store->id ?>">Edit</a>
                    </div>
                    <div>
                        <a href="/?page=stores/stock&id=<?= $store->id ?>">Stock</a>
                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>