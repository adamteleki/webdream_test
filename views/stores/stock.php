<?php

use models\Product;
use models\Stock;
use models\Store;

$id = null;
if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
}

$stockModel = new Stock;

if (isset($_POST) && !empty($_POST)) {
    if (isset($_POST['quantity_plus'])) {
        if ($stockModel->addStock($_POST['store_id'])) {
            header('Location: /?page=stores/index');
        }
    } elseif (isset($_POST['quantity_minus'])) {
        if ($stockModel->createProduct($_POST['store_id'])) {
            header('Location: /?page=stores/index');
        }
    }

}

$stocks = $stockModel->getStockByStoreId($id);

?>
<div class="">
    <h3>Stocks</h3>

    <br/>
    <table style="width:100%" border="collapse">
        <tr>
            <th>ID</th>
            <th>Product</th>
            <th>Store</th>
            <th>Quantity</th>
            <th></th>
        </tr>
        <?php
        foreach ($stocks as $key => $stock) {
            $store_name = '';
            $product_name = '';

            $storeModel = new Store;
            if (!is_null($storeModel) && !empty($storeModel)) {
                $store_name = $storeModel->getStoreById($stock->store_id)->name;
            }

            $productModel = new Product;
            if (!is_null($productModel) && !empty($productModel)) {
                $product_name = $productModel->getProductById($stock->product_id)->name;
            }

            ?>
            <tr>
                <td><?= $stock->id ?></td>
                <td><?= $product_name ?></td>
                <td><?= $store_name ?></td>
                <td><?= number_format($stock->quantity) ?></td>
                <td>
                    <div>
                        <form method="POST">
                            <input type="hidden" name="store_id" required value="<?= $stock->store_id ?>">
                            <input type="hidden" name="product_id" required value="<?= $stock->product_id ?>">
                            <input type="hidden" name="stock_id" required value="<?= $stock->id ?>">
                            <input type="number" min="1" required placeholder="1" name="quantity_plus"
                                   style="text-align: center;width:50px;">
                            <input type="submit" value="+"
                                   style="background-color:forestgreen;width:30px;height:30px;font-size:15px;font-weight:bold;">
                        </form>
                    </div>
                    <br>
                    <div>
                        <form method="POST">
                            <input type="hidden" name="store_id" required value="<?= $stock->store_id ?>">
                            <input type="hidden" name="product_id" required value="<?= $stock->product_id ?>">
                            <input type="hidden" name="stock_id" required value="<?= $stock->id ?>">
                            <input type="number" min="1" required placeholder="1" name="quantity_minus"
                                   style="text-align: center;width:50px;">
                            <input type="submit" value="-"
                                   style="background-color:orangered;width:30px;height:30px;font-size:15px;font-weight:bold;">
                        </form>
                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>