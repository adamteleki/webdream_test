<?php

use models\Brand;

$id = isset($_GET['id']) && !empty($_GET['id']) && !is_null($_GET['id']) ? $_GET['id'] : null;
$brandObject = new Brand;
$brand = $brandObject->getBrandById($id);

if (!empty($_POST) && !is_null($_POST)){
    if ($brandObject->updateBrand($id)) {
        header('Location: /?page=brands/index');
    }
}else
?>
<div>
    <form method="POST">
        Name:<br>
        <input type="text" name="name" value="<?= $brand->name ?>" required minlength="2">
        <br>
        Quality:<br>
        <input type="number" name="quality" value="<?= $brand->quality ?>" required min="1" max="5">
        <br><br>
        <input type="submit" value="Submit">
    </form>
</div>