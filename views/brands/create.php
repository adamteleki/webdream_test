<?php

use models\Brand;

if (!empty($_POST) && !is_null($_POST)) {
    $brand = new Brand;
    if ($brand->createBrand()) {
        header('Location: /?page=brands/index');
    }
}
?>
<div>
    <form method="POST">
        Name:<br>
        <input type="text" name="name" required minlength="2">
        <br>
        Quality:<br>
        <input type="number" name="quality" required min="1" max="5">
        <br><br>
        <input type="submit" value="Submit">
    </form>
</div>