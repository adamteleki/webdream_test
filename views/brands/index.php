<?php

use models\Brand;

$brandModel = new Brand;
$brands = $brandModel->getBrands();
?>
<div class="">
    <h3>Brands</h3>
    <div>
        <a href="/?page=brands/create">New Brand</a>
    </div>
    <br/>
    <table style="width:100%" border="collapse">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Quality</th>
            <th></th>
        </tr>
        <?php
        foreach ($brands as $key => $brand) {
            ?>
            <tr>
                <td><?= $brand->id ?></td>
                <td><?= $brand->name ?></td>
                <td><?= $brand->quality ?></td>
                <td>
                    <div>
                        <a href="/?page=brands/update&id=<?= $brand->id ?>">Edit</a>
                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>