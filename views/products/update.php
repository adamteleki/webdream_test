<?php

use models\Brand;
use models\Product;

$id = isset($_GET['id']) && !empty($_GET['id']) && !is_null($_GET['id']) ? $_GET['id'] : null;
$productModel = new Product;
$product = $productModel->getProductById($id);

$brandModel = new Brand;
$brands = $brandModel->getBrands();

if (!empty($_POST) && !is_null($_POST)){
    if ($productModel->updateProduct($id)) {
        header('Location: /?page=products/index');
    }
}else
?>
<div>
    <form method="POST">
        Name:<br>
        <input type="text" name="name" required minlength="2" value="<?= $product->name ?>">
        <br>
        SKU:<br>
        <input type="text" name="sku" required minlength="2" value="<?= $product->sku ?>">
        <br>
        Price:<br>
        <input type="number" name="price" required min="1" value="<?= $product->price ?>">
        <br>
        Brands:<br>
        <select name="brand" required>
            <?php
            foreach ($brands as $brand) {
                $selected = '';
                if ($product->brand_id == $brand->id) {
                    $selected = 'selected';
                }
                ?>
                <option value="<?= $brand->id ?>" <?= $selected ?>><?= $brand->name ?></option>
                <?php
            }
            ?>
        </select>
        <br><br>
        <input type="submit" value="Submit">
    </form>
</div>