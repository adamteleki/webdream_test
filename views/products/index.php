<?php

use models\Brand;
use models\Product;

$productModel = new Product;
$products = $productModel->getProducts();
?>
<div class="">
    <h3>Products</h3>
    <div>
        <a href="/?page=products/create">New Product</a>
    </div>
    <br/>
    <table style="width:100%" border="collapse">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>SKU</th>
            <th>Brand</th>
            <th>Price</th>
            <th></th>
        </tr>
        <?php
        foreach ($products as $key => $product) {
            $brandModel = new Brand;
            $brand_name = '';
            if (!is_null($brandModel) && !empty($brandModel)) {
                $brand_name = $brandModel->getBrandById($product->brand_id)->name;
            }
            ?>
            <tr>
                <td><?= $product->id ?></td>
                <td><?= $product->name ?></td>
                <td><?= $product->sku ?></td>
                <td><?= $brand_name ?></td>
                <td><?= $productModel->priceFormat($product->price) ?></td>
                <td>
                    <div>
                        <a href="/?page=products/update&id=<?= $product->id ?>">Edit</a>
                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>