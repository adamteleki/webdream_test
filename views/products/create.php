<?php

use models\Brand;
use models\Product;

$brandModel = new Brand;
$brands = $brandModel->getBrands();

if (!empty($_POST) && !is_null($_POST)) {
    $product = new Product;
    if ($product->createProduct()) {
        header('Location: /?page=products/index');
    }
}
?>
<div>
    <form method="POST">
        Name:<br>
        <input type="text" name="name" required minlength="2">
        <br>
        SKU:<br>
        <input type="text" name="sku" required minlength="2">
        <br>
        Price:<br>
        <input type="number" name="price" required min="1">
        <br>
        Brands:<br>
        <select name="brand" required>
            <?php foreach ($brands as $brand) { ?>
                <option value="<?= $brand->id ?>"><?= $brand->name ?></option>
            <?php } ?>
        </select>
        <br><br>
        <input type="submit" value="Submit">
    </form>
</div>