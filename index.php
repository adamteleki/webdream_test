<?php
session_start();
include('core/autoload.php');

?>

<!DOCTYPE html>
<html>
<head>
    <title>Teleki Ádám - Teszt feladat</title>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

        .col-md-6 {
            float: left;
            width: 48%;
            padding-left: 15px;
            padding-right: 15px;
        }

        #nav ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            width: 100%;
            background-color: #f1f1f1;
            border: 1px solid #555;
            display: table;
        }

        #nav li a {
            display: block;
            color: #000;
            padding: 8px 16px;
            text-decoration: none;
        }

        #nav li {
            text-align: center;
            float: left;
        }

        #nav li:last-child {
            border-bottom: none;
        }

        #nav li a.active {
            background-color: #4CAF50;
            color: white;
        }

        #nav li a:hover:not(.active) {
            background-color: #555;
            color: white;
        }

        table ul {
            list-style: none;
        }
    </style>
</head>
<body>

<div id="nav">
    <ul>
        <li>
            <a href="/?page=stores/index">Stores</a>
        </li>
        <li>
            <a href="/?page=brands/index">Brands</a>
        </li>
        <li>
            <a href="/?page=products/index">Products</a>
        </li>
    </ul>
</div>
<div id="content" style="padding:20px 0;">
    <?php
    /*print '<pre>';
    print_r($_SESSION['database']['stores']);
    print '</pre>';*/
    ?>
    <?php
    if (isset($_GET['page']) && !empty($_GET['page'])) {
        if (!@include 'views/' . $_GET['page'] . '.php') {
            include '404.php';
        }

    } else {
        include 'views/stores/index.php';
    }
    ?>

</div>


</body>
</html>