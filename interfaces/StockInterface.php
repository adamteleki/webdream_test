<?php

namespace interfaces;

interface StockInterface
{

    public function addStock($id);

    public function removeStock($id);

}