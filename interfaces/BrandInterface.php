<?php

namespace interfaces;

interface BrandInterface
{

    public function createBrand();

    public function updateBrand($id);

}