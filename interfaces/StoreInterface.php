<?php

namespace interfaces;

interface StoreInterface
{
    public function createStore();

    public function updateStore($id);

}