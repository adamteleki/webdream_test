<?php

namespace interfaces;

interface ProductInterface
{

    public function createProduct();

    public function updateProduct($id);

}