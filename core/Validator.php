<?php

namespace core;

class Validator
{
    private static $rules = [
        'string' => 'isString',
        'integer' => 'isInteger',
        'min' => 'minLength',
        'max' => 'maxLength',
        'minInt' => 'minInteger',
        'maxInt' => 'maxInteger'
    ];

    public static $errors = [];

    public static function validate($rules, $values)
    {
        foreach ($rules as $rule) {
            $data_names = $rule[0];
            $data_rules = $rule[1];

            $returnData = false;
            foreach ($data_names as $key => $name) {
                $value = $values[$name];
                foreach ($data_rules as $key2 => $rule) {
                    $length = null;
                    $rule_name = $rule;

                    if (stristr($rule, ':')) {
                        $exp = explode(':', $rule);
                        $length = $exp[1];
                        $rule_name = $exp[0];
                    }

                    $rulesData = self::$rules;
                    $ruleset = isset($rulesData[$rule_name]) ? $rulesData[$rule_name] : null;
                    switch ($ruleset) {
                        case "isString":
                            $returnData = self::isString($value, $name);
                            break;
                        case "isInteger":
                            $returnData = self::isInteger($value, $name);
                            break;
                        case "minLength":
                            $returnData = self::minLength($value, $name, $length);
                            break;
                        case "maxLength":
                            $returnData = self::maxLength($value, $name, $length);
                            break;
                        case "minInteger":
                            $returnData = self::minInteger($value, $name, $length);
                            break;
                        case "maxInteger":
                            $returnData = self::maxInteger($value, $name, $length);
                            break;
                        default:
                            self::$errors[] = 'Invalid ruleset!';
                    }

                }
            }
        }

        if (!empty(self::$errors) && !is_null(self::$errors)) {
            return self::$errors;
        }

        return $returnData;
    }

    public static function valid($value, $param, $rule, $length = 3)
    {
        $ruleset = isset(self::$rules[$rule]) ? self::$rules[$rule] : null;
        switch ($ruleset) {
            case "isString":
                $returnData = self::isString($value, $param);
                break;
            case "isInteger":
                $returnData = self::isInteger($value, $param);
                break;
            case "minLength":
                $returnData = self::minLength($value, $param, $length);
                break;
            case "maxLength":
                $returnData = self::maxLength($value, $param, $length);
                break;
            default:
                self::$errors[] = 'Invalid ruleset!';
        }

        if (!empty(self::$errors) && !is_null(self::$errors)) {
            return self::$errors;
        }
        return $returnData;
    }

    protected function isString($value, $param)
    {
        if (!is_string($value)) {
            self::$errors[] = $param . ' is not a valid string!';
            return false;
        }
        return true;
    }

    protected function isInteger($value, $param)
    {
        if (!is_numeric($value)) {
            self::$errors[] = $param . ' is not a valid integer!';
            return false;
        }
        return true;
    }

    protected function minInteger($value, $param, $length)
    {
        if (is_numeric($value)) {
            if ($value < $length) {
                self::$errors[] = $param . ' must not be lower than ' . $length . ' !';
                return false;
            }
        } else {
            self::$errors[] = $param . ' is not a valid integer!';
            return false;
        }
        return true;
    }

    protected function maxInteger($value, $param, $length)
    {
        if (is_numeric($value)) {
            if ($value > $length) {
                self::$errors[] = $param . ' must not be bigger than ' . $length . ' !';
                return false;
            }
        } else {
            self::$errors[] = $param . ' is not a valid integer!';
            return false;
        }
        return true;
    }

    protected function minLength($value, $param, $length)
    {
        if (strlen($value) < $length) {
            self::$errors[] = $param . ' must not be lower than ' . $length . ' characters!';
            return false;
        }
        return true;
    }

    protected function maxLength($value, $param, $length)
    {
        if (strlen($value) > $length) {
            self::$errors[] = $param . ' must not be bigger than ' . $length . ' characters!';
            return false;
        }
        return true;
    }
}
