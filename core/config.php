<?php
$database = [
    'stores' => [
        ['id' => 1, 'name' => 'Store_1', 'capacity' => 50],
        ['id' => 2, 'name' => 'Store_2', 'capacity' => 50],
        ['id' => 3, 'name' => 'Store_3', 'capacity' => 50],
    ],
    'brands' => [
        ['id' => 1, 'name' => 'Brand_1', 'quality' => 1],
        ['id' => 2, 'name' => 'Brand_2', 'quality' => 2],
        ['id' => 3, 'name' => 'Brand_3', 'quality' => 3],
        ['id' => 4, 'name' => 'Brand_4', 'quality' => 4],
        ['id' => 5, 'name' => 'Brand_5', 'quality' => 5],
    ],
    'products' => [
        ['id' => 1, 'name' => 'Product_1', 'sku' => '00000001', 'brand_id' => 1, 'price' => 3000],
        ['id' => 2, 'name' => 'Product_2', 'sku' => '00000002', 'brand_id' => 2, 'price' => 1000],
        ['id' => 3, 'name' => 'Product_3', 'sku' => '00000003', 'brand_id' => 4, 'price' => 2000],
        ['id' => 4, 'name' => 'Product_4', 'sku' => '00000004', 'brand_id' => 5, 'price' => 4000],
        ['id' => 5, 'name' => 'Product_5', 'sku' => '00000005', 'brand_id' => 2, 'price' => 8000],
        ['id' => 6, 'name' => 'Product_6', 'sku' => '00000006', 'brand_id' => 1, 'price' => 1500],
        ['id' => 7, 'name' => 'Product_7', 'sku' => '00000007', 'brand_id' => 3, 'price' => 4000],
        ['id' => 8, 'name' => 'Product_8', 'sku' => '00000008', 'brand_id' => 4, 'price' => 6700],
        ['id' => 9, 'name' => 'Product_9', 'sku' => '00000009', 'brand_id' => 5, 'price' => 4900],
    ],
    'inventory' => [
        ['id' => 1, 'store_id' => 1, 'product_id' => 1, 'quantity' => 10],
        ['id' => 2, 'store_id' => 1, 'product_id' => 2, 'quantity' => 10],
        ['id' => 3, 'store_id' => 2, 'product_id' => 1, 'quantity' => 10],
        ['id' => 4, 'store_id' => 2, 'product_id' => 2, 'quantity' => 10],
        ['id' => 5, 'store_id' => 2, 'product_id' => 3, 'quantity' => 10],
        ['id' => 6, 'store_id' => 3, 'product_id' => 6, 'quantity' => 22],
        ['id' => 7, 'store_id' => 2, 'product_id' => 9, 'quantity' => 16],
        ['id' => 8, 'store_id' => 3, 'product_id' => 2, 'quantity' => 13],
        ['id' => 9, 'store_id' => 1, 'product_id' => 4, 'quantity' => 5],
        ['id' => 10, 'store_id' => 3, 'product_id' => 5, 'quantity' => 23],
        ['id' => 11, 'store_id' => 1, 'product_id' => 6, 'quantity' => 4],
        ['id' => 12, 'store_id' => 1, 'product_id' => 9, 'quantity' => 4],
    ]
];
if (!isset($_SESSION['database'])) {
    $_SESSION['database'] = $database;
} else {
    //TODO: Kivenni ezt a részt ha már nincs teszt
    /*unset($_SESSION['database']);
    $_SESSION['database'] = $database;*/
}

